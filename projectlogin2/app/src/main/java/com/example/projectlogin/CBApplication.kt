package com.example.projectlogin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CBApplication : Application()